/*Stwórz program w pliku .c, w którym będzie main() i który będzie miał załączony plik
moduly.h. Program ma być w stanie obsłużyć (wybór użytkownika w switch - case):
• dodanie nowego klienta na koniec listy (użytkownik podaje nazwisko)
• usunięcie klienta o danym nazwisku (uzupełnić pop by surname() w module)
• wyświetlenie ilości klientów
• wyświetlenie nazwisk klientów
- Stwórz plik Makefile, który skompiluje program wraz z modułem.*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "moduly.h"

int main()
{

    char surname[30]="";
    struct Client *head;
    head = (struct Client*)malloc(sizeof(struct Client));
    strcpy(head->surname,"Lach");
    head->next = NULL;

    int wybor = 0;

    while(1)
    {
        while(wybor<1 || wybor>5){
            printf("Co chcesz wykonac?\n1. Dodaj nowego klienta \n2. Usun klienta \n3. Wyswietl ilosc klientow \n4. Wyswietl nazwiska klientow. \n5. Zakoncz program\n");
            scanf("%d", &wybor);
        }


    switch(wybor)
    {
        case 1: //Dodanie klienta
            printf("Podaj nazwisko: ");
            scanf("%s", surname);
            push_back(&head,surname);
        break;

        case 2: //usuniecie klienta
            wybor = 0;
            printf("Usun klienta:\n1. Po nazwisku\n2. Po indeksie\n");
            scanf("%d", &wybor);
            switch(wybor)
            {
                case 1:
                    surname[0] = '\0';
                    printf("Podaj nazwisko: ");
                    scanf("%s",surname);
                    pop_by_surname(&head,surname);
                break;

                case 2:
                    printf("Podaj indeks: ");
                    scanf("%d", &index);
                    pop_by_index(&head,index);
            }
            
        break;

        case 3: //ilosc klientow
            printf("Liczba klientow: %d\n ", list_size(head));
        break;

        case 4: //lista klientow
            show_list(head);
        break;

        case 5: //zakonczenie programu
            free(head);
            exit(1);
        break;

    }
    wybor = 0;
    }
    

    free(head);
    return 0;
}
