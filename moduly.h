#include <stdio.h>

struct Client
{
    char surname[30];
    struct Client *next;
};

int list_size(struct Client *head); //return size of a list
void show_list(struct Client *head);    //prints a list
void push_front(struct Client **head, char *p_surname); //Add an element to the beginning of a list (new head)
void push_back(struct Client **head, char *p_surname);  //Add an element to the end of a list (new tail)
void push_by_index(struct Client **head, char *p_surname, int p_index); //Add element by index
void pop_front(struct Client **head);   //Delete first element of a list (head)
void pop_by_index(struct Client **head, int p_index);   //Delete element by index
void pop_by_surname(struct Client **head, char *p_surname); //Delete first element of given name
void pop_back(struct Client **head);    //Delete last element of a list (tail)